// File: Gulpfile.js
'use strict';

var gulp = require('gulp');
var connect = require('gulp-connect');
var stylus = require('gulp-stylus');
var nib = require('nib');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var inject = require('gulp-inject');
var wiredep = require('wiredep').stream;
var gulpif = require('gulp-if');
var minifyCss = require('gulp-minify-css');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var uncss = require('gulp-uncss');

// Servidor web de desarrollo
gulp.task('server', function() {
  connect.server({
    root: 'app',
    hostname: '0.0.0.0',
    port: 8080,
    livereload: true,
  });
});

// Servidor web de produccion
gulp.task('server-dist', function() {
  connect.server({
    root: 'dist',
    hostname: '0.0.0.0',
    port: 8080,
    livereload: true,
  });
});

// Busca errores en el JS y muestra en terminal
gulp.task('jshint', function() {
  return gulp.src('./app/scripts/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

// preprocesa archivos stylus a CSS y recarga los cambios
gulp.task('css', function() {
  gulp.src('./app/stylesheets/main.styl')
  .pipe(stylus({ use: nib(),
                 'include css': true }))
    .pipe(gulp.dest('./app/stylesheets'))
    .pipe(connect.reload());
});

//recarga el navegador cuando hay cambios en el html
gulp.task('html', function() {
  gulp.src('./app/*.html')
    .pipe(connect.reload());
});

// Busca en las carpetas los estilos y js para inyectarlos
gulp.task('inject', function() {
  return gulp.src('index.html', {cwd: './app'})
    .pipe(inject(
      gulp.src(['./app/scripts/*.js']), {
        read: false,
        ignorePath: '/app'
      }))
    .pipe(inject(
      gulp.src(['./app/stylesheets/*.css']), {
        read: false,
        ignorePath: '/app'
      }))
    .pipe(gulp.dest('./app'));
});

// Inyecta las librerias de bower
gulp.task('wiredep', function() {
  gulp.src('./app/index.html')
    .pipe(wiredep({
      directory: './app/lib'
    }))
    .pipe(gulp.dest('./app'));
});

// Comprime los CSS y JS y los minifica
gulp.task('compress', function() {
  gulp.src('./app/index.html')
    .pipe(useref.assets())
    .pipe(gulpif('*.js', uglify({mangle: false})))
    .pipe(gulpif('*.css', minifyCss()))
    .pipe(gulp.dest('./dist'));
});

// Elimina el css que no es utilizado
gulp.task('uncss', function() {
  gulp.src('./dist/css/main.min.css')
    .pipe(uncss({
      html: ['./app/index.html']
    }))
    .pipe(gulp.dest('./dist/css'));
});

// Copia el contenido de los estaticos y html al directorio
gulp.task('copy', function() {
  gulp.src('./app/index.html')
    .pipe(useref())
    .pipe(gulp.dest('./dist'));
  gulp.src('./app/lib/fontawesome/fonts/**')
    .pipe(gulp.dest('./dist/fonts'));
});

// Vigila el codigo y lanza las tareas
gulp.task('watch', function() {
  gulp.watch(['./app/*.html'], ['html']);
  gulp.watch(['./app/stylesheets/*.styl'], ['css', 'inject']);
  gulp.watch(['./app/scripts/*.js', './Gulpfile.js'], ['jshint', 'inject']);
  gulp.watch(['./bower.json'], ['wiredep']);
});

gulp.task('default', ['server', 'inject', 'wiredep', 'watch']);
gulp.task('build', ['compress', 'copy', 'uncss']);
